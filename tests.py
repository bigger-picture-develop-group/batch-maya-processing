import codecs
import sys
import os
import time
import zipfile

try:
    import pymel.core as pm
    from maya_cmds import scan_arnold_udim_textures
    from init_arnold import *

    MAYA_ENV = True
except ImportError:
    print("\n\"Not in Maya!!\"\n")
    MAYA_ENV = False


def test_udim_scan(src):
    if not MAYA_ENV:
        return

    if os.path.isfile(src) and ".ma" in src:
        pm.openFile(src)

        file_list = scan_arnold_udim_textures()
        print len(file_list)
        for i in file_list:
            print i


def test_file_size(list_file):
    with codecs.open(list_file, "r", encoding="utf8") as reader:
        file_list = [item.strip("\r\n") for item in reader.readlines()]

    size_list = []

    for item in file_list:
        size_list.append((item, os.stat(item).st_size))

    sorted_list = sorted(size_list, key=lambda each: each[1], reverse=True)
    for item in sorted_list:
        print item[0], ":" + " " * (40 - len(os.path.basename(item[0]))) + \
                       "{:,} MBytes".format(float(item[-1] / 1000000))

    return file_list


def test_zip_big_file():
    big_file = "P:/TBF/asset/set/sw_out/Surface/work/maya/sw_out_ryan_sh0060_FGa.ma"
    with zipfile.ZipFile("C:/Collects/test_py_zip.zip", 'w', allowZip64=True) as zipper:
        zipper.write(big_file)


def test_zip_big_file2(list_file):
    file_list = test_file_size(list_file)

    for item in file_list:
        try:
            basename = os.path.basename(item)
            print (u"Zipping {}".format(item))
            with zipfile.ZipFile(u"C:/Collects/{}.zip".format(basename), 'w', allowZip64=True) as zipper:
                zipper.write(item)
        except AttributeError as err:
            print(err)


def test_zip_big_file3(list_file, dst_path):
    begin = time.time()
    file_list = test_file_size(list_file)

    for item in file_list:
        try:
            print (u"Zipping {}".format(item))
            with zipfile.ZipFile(unicode(dst_path), 'a', allowZip64=True) as zipper:
                zipper.write(item)
        except AttributeError as err:
            print(err)

    print("Zip Duration: {}".format(time.time() - begin))

if __name__ == "__main__":
    args = sys.argv
    if len(args) > 1:
        # test_udim_scan(args[1])
        test_zip_big_file3(args[1], args[2])
    else:
        test_zip_big_file()
