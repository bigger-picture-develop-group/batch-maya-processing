import codecs
import locale
import os
import subprocess
import sys
import time

from maya_cmds import list_maya_file


def archive_from_text_file(text_file):
    with codecs.open(text_file, "r", encoding="utf8") as reader:
        file_list = reader.readlines()

    file_list = [item.strip("\r\n") for item in file_list]

    dst = os.path.join(os.path.dirname(text_file), "final_archive2.zip")
    cmd = u"\"C:\\Program Files\\7-Zip\\7z.exe\" a "

    for _file in file_list:
        cmd += u"{dst} \"{src}\"".format(dst=dst, src=_file)
        cmd += " -spf -tzip"
        subprocess.call(cmd.encode(locale.getpreferredencoding()), shell=True)


def archive_from_text_file2(text_file, dst_name):
    with codecs.open(text_file, "r", encoding="utf8") as reader:
        file_list = reader.readlines()

    file_list = [item.strip("\r\n") for item in file_list]

    dst = os.path.join(os.path.dirname(text_file), dst_name)

    length = len(file_list)
    step = 80
    loop = (length / step) + 1

    init = 0

    begin = time.time()

    for each in range(loop):
        cmd = u"\"C:\\Program Files\\7-Zip\\7z.exe\" a {dst} ".format(dst=dst)

        for _file in file_list[init:init + step]:
            cmd += u" \"{src}\" ".format(src=_file)

        cmd += " -spf -tzip"
        init += step

        subprocess.call(cmd.encode(locale.getpreferredencoding()), shell=True)

    duration = time.time() - begin
    print "Total Duration: {}".format(duration)


def check_7zip_executable():
    # try if 7z.exe is in path
    # else we try explict path
    # all else we fail

    explict_path_win = "C:\\Program Files\\7-Zip\\7z.exe"

    if os.path.isfile(explict_path_win):
        return explict_path_win
    else:
        raise WindowsError("7z.exe is not found in the system")


def archive(maya_file, dst_path, step=80):

    executable = check_7zip_executable()

    # make the dir if not exists
    if not os.path.isdir(os.path.dirname(dst_path)):
        os.makedirs(os.path.dirname(dst_path))

    begin = time.time()

    file_list = list_maya_file(maya_file)

    maya_open_time = time.time() - begin

    begin_zip = time.time()

    # list files to archive in steps
    length = len(file_list)
    loop = (length / step) + 1

    init = 0

    for each in range(loop):
        cmd = u"\"{cmd}\" a {dst} ".format(cmd=executable, dst=dst_path)

        for _file in file_list[init:init + step]:
            cmd += u" \"{src}\" ".format(src=_file)

        cmd += " -spf -tzip"
        init += step

        subprocess.call(cmd.encode(locale.getpreferredencoding()), shell=True)

    duration = time.time() - begin_zip
    print "Maya Open Duration: {}".format(maya_open_time)
    print "Archive Duration: {}".format(duration)


if __name__ == "__main__":
    args = sys.argv
    archive_from_text_file2(args[1], args[2])
