# Maya file batch processing tools

### Why
In production we often need to process Maya scene files without opening using the GUI. 

### What
1. Package assets for different office and contractors. 

### How
mayapy (2.7)

### TODO
1. Parse file reference to text file or serialize to json
2. Run texture processing tools in background (e.g., maketx, Redshift's TextureProcessor.exe, mentalray)

